package ap.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcStudent {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/College";
	// Database credentials
	static final String USER = "root";
	static final String PASS = "root";

	public static void SampleConnection() {
		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);
			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating table in given database...");

			 stmt = conn.createStatement();
			 String sql1 = "drop table Student;";
			 stmt.executeUpdate(sql1);
			stmt = conn.createStatement();

			String sql = "CREATE TABLE Student (id int not NULL, first VARCHAR(255), last VARCHAR(255), subject varchar(20), marks int,PRIMARY KEY ( id ));";

			stmt.executeUpdate(sql);
			System.out.println("Created table in given database...");
			System.out.println("Inserting records into the table...");
			stmt = conn.createStatement();
			sql = "INSERT INTO Student VALUES (100, 'Kriss', 'Kurian', 'maths',100);";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO Student VALUES (101, 'Enrique', 'John', 'maths',25);";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO Student VALUES (102, 'Taylor', 'Swift','maths', 30);";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO Student VALUES (103, 'Linkin', 'Park', 'maths',28);";
			stmt.executeUpdate(sql);
			System.out.println("Inserted records into the table...");

			sql = "SELECT id, first, last, subject,marks FROM Student ";
			ResultSet rs = stmt.executeQuery(sql);
			// STEP 5: Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				int id = rs.getInt("id");
				int marks = rs.getInt("marks");
				String first = rs.getString("first");
				String last = rs.getString("last");
				String subject = rs.getString("subject");
				// Display values
				System.out.print("ID: " + id);
				System.out.print(", First: " + first);
				System.out.println(", Last: " + last);
				System.out.println(", subject: " + subject);
				System.out.println(", Marks: " + marks);
			}
			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
			System.out.println("Goodbye!");
		}
	}
}