
public class ArrayD {
	
		public static void main(String[] args) {
			int[] AnneMarks = { 75, 90, 100, 78, 100 };
			int[] BredMarks = { 100, 95, 90, 80, 95 };
			int[] LeeMarks = { 95, 100, 90, 80, 70 };
			int[] KumarMarks = { 90, 90, 75, 80, 70 };
			int[] RaviMarks = { 80, 85, 80, 75, 70 };

			System.out.println("Total of Anne marks is :" + MarksOpp.findTotal(AnneMarks));
			System.out.println("maximum of Anne marks is :" + MarksOpperation.findmax(AnneMarks));
			System.out.println("Minimum of Anne marks is :" + MarksOpperation.findmin(AnneMarks));
			System.out.println("Average of Anne marks is :" + MarksOpperation.findAve(AnneMarks));
			System.out.println("*-------------------------*");

			System.out.println("Total of bred marks is :" + MarksOpperation.findTotal(BredMarks));
			System.out.println("maximum of bred marks is :" + MarksOpperation.findmax(BredMarks));
			System.out.println("Minimum of bred marks is :" + MarksOpperation.findmin(BredMarks));
			System.out.println("Average of bred marks is :" + MarksOpperation.findAve(BredMarks));
			System.out.println("*-------------------------*");

			System.out.println("Total of Lee marks is :" + MarksOpperation.findTotal(LeeMarks));
			System.out.println("maximum of Lee marks is :" + MarksOpperation.findmax(LeeMarks));
			System.out.println("Minimum of Lee marks is :" + MarksOpperation.findmin(LeeMarks));
			System.out.println("Average of Lee marks is :" + MarksOpperation.findAve(LeeMarks));
			System.out.println("*-----------------------------*");

			System.out.println("Total of Kumar marks is :" + MarksOpperation.findTotal(KumarMarks));
			System.out.println("maximum of Kumar  marks is :" + MarksOpperation.findmax(KumarMarks));
			System.out.println("Minimum of Kumar  marks is :" + MarksOpperation.findmin(KumarMarks));
			System.out.println("Average of Kumar  marks is :" + MarksOpperation.findAve(KumarMarks));
			System.out.println("*---------------------------*");

			System.out.println("Total of Ravi marks is :" + MarksOpperation.findTotal(RaviMarks));
			System.out.println("maximum of Ravi marks is :" + MarksOpperation.findmax(RaviMarks));
			System.out.println("Minimum of Ravi marks is :" + MarksOpperation.findmin(RaviMarks));
			System.out.println("Average of Ravi marks is :" + MarksOpperation.findAve(RaviMarks));
			System.out.println("*---------------------------*");

		}
	}
}
