package bcas.oop.encap;

public class StaticBlock {
	static{
		System.out.println("Content from static-block");
		int x = 2;
		int y = 5;
		System.out.println(x+y);
	}
	public static void main(String[] args) {
		System.out.println("Message from main method");
		
		int x = 2;
		int y = 5;
		System.out.println(x+y);
	}

}
