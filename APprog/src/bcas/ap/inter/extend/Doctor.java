package bcas.ap.inter.extend;

public class Doctor extends Person {
	String department;
	int doct_id;
	String work_time;
	
	public Doctor(String name, int age,String contact_no,String department,int doct_id,String work_time) {
		
		super( name, age,contact_no);
		
		this.department=department;
		this.doct_id=doct_id;
		this.work_time=work_time;
		
	}
}
