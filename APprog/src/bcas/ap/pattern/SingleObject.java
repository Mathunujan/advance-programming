package bcas.ap.pattern;

	  public class SingleObject {
	      
	      private static final SingleObject instance = new SingleObject();
	      
	      //private constructor to avoid client applications to use constructor
	      private SingleObject(){}

	      public static SingleObject getInstance(){
	          return instance;
	      }
	  }
	 
	
	


