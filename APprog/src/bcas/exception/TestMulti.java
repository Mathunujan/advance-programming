package bcas.exception;

public class TestMulti {

	static void fun() {
		try {
			// throw new NullPointerException("demo");
			String s = null;
			System.out.println(s.length());// NullPointerException
			int a = 5;
			int b = 0;
			int Total = a / b;

			System.out.println(Total);

			
		} catch (NullPointerException  e) {
			System.out.println("Caught inside fun().");
		}
			catch (ArithmeticException e) {
				System.out.println("Caught in main.");
			}

			// throw e; // rethrowing the exception
		
	}

	public static void main(String args[]) {
		//try {
			fun();
		//} catch (ArithmeticException e) {
			//System.out.println("Caught in main.");
		}
	}

