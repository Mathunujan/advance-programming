package arrayWorks.shelf.Readvalue;

import java.util.Scanner;

import arrayWorks.shelf.pm.PackMessages;

public class ReadValue {
	Scanner sc = new Scanner(System.in);
	
	public int readsInt(String msg) {
		int num;
		System.out.println(msg);
		num = sc.nextInt();
		
		return num;		
		
	}
	
	public String readString(String msg) {
		String value;
		System.out.println(msg);
		value=sc.nextLine();
		return value;
		
	}
	
	public int[] readtwoValues(String msg) {
		int [] twoValues=new int[2];
		String value = readString(msg);
		Scanner sc2 = new Scanner(value).useDelimiter(",");
		
		twoValues[0]=sc2.nextInt();
		twoValues[1]=sc2.nextInt();
		sc2.close();
		
		return twoValues;
		
	}
	public boolean cont() {
		String dec="";
		System.out.println(PackMessages.Continue);
		 dec=sc.nextLine();
		if(dec.equals("y")){
			return true;		
		}
		else {
		return false;
		}
		
	}

}
