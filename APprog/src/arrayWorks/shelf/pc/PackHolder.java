package arrayWorks.shelf.pc;

import arrayWorks.shelf.pm.PackMessages;

public class PackHolder {
	int row,col;
	int maxHoles;
	int holes[][];
public PackHolder(int row,int col) {
	this.row = row;
	this.col=col;
	maxHoles = this.col*this.row;	
	createHole();
}

public void createHole() {
	holes= new int[row][col];	
}

public String placepack(int holeNo) {
	int tokenRow=0;
	int tokenCol=0;
	
	int mul=(holeNo/col);
	if (mul*col==holeNo) {
		tokenRow=((holeNo/col)-1) ;	
		tokenCol=col-1;	
	}else {
		 tokenRow=(holeNo/col) ;
	     tokenCol=(holeNo%col)-1;
	}
	
	if(checkFree(tokenRow,tokenCol)) {
		holes[tokenRow][tokenCol] = holeNo;
		return PackMessages.success;	
	}
	return PackMessages.notFree;
	
}

public String checkAvaliable(int holeNo) {
	if (maxHoles>=holeNo) {
		return placepack(holeNo);
	}
	else
	return PackMessages.tokenNo;
	
}
public boolean checkFree(int tokenRow, int tokenCol) {
	holes[0][1]=25;
	
	if (holes[tokenRow][tokenCol]==0) {
	return true ;
	}
	else {
	return false;
	}
	
}

}

