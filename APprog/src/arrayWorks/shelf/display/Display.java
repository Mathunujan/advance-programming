package arrayWorks.shelf.display;

public class Display {

	public void drawArray(int row, int col) {
		int array[][] = new int[row][col];

		for (int x = 0; x < row; x++) {
			for (int y = 0; y < col; y++) {
				System.out.println(array[x][y] + " ");
			}
			System.out.println();

		}
	}
}
