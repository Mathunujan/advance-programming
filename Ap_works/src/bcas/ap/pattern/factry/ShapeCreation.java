package bcas.ap.pattern.factry;

public class ShapeCreation {
	public static Shape createShape(ShapeType type) {
		Shape shape = null;
		switch (type) {
		case Circle:
			shape = new Circle();

			break;
		case Square:
			shape = new SquareShape();

			break;
		case Rectangle:
			shape = new RectangleShape();

			break;

		default:
			break;
		}
		return shape;
		
	}

}
