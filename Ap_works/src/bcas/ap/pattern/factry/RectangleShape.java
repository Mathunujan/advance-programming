package bcas.ap.pattern.factry;

public class RectangleShape extends Shape{

	public RectangleShape() {
		super(ShapeType.Rectangle);
		construct();
		
	}

	@Override
	protected void construct() {
		System.out.println("Drawing RectangleShape");
		
	}
	
	
	

}
