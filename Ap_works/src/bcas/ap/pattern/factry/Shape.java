package bcas.ap.pattern.factry;

public abstract class Shape {
	private ShapeType type = null;

	public Shape(ShapeType Type) {
		this.type = Type;
		arrangeParts();

	}

	private void arrangeParts() {
		System.out.println("Calculating Equation for "+type);

	}
	protected  abstract void construct();

	public ShapeType getModel() {
		return type;
	}

	public void setModel(ShapeType type) {
		this.type = type;
	}
	

}
