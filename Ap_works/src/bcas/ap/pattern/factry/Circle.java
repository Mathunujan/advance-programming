package bcas.ap.pattern.factry;

public class Circle extends Shape{
	public Circle() {
		super(ShapeType.Circle);
		construct();
		
	}

	@Override
	protected void construct() {
		System.out.println("Drawing Circle");
		
	}
	

}
