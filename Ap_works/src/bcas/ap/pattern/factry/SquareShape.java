package bcas.ap.pattern.factry;

public class SquareShape extends Shape  {
	public SquareShape() {
		super(ShapeType.Square);
		construct();
		
	}

	@Override
	protected void construct() {
		System.out.println("Drawing Square");
		
	}
	
	

}
