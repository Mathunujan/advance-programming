package ap.wrk.hwre;

public class Invoice {

	String partNo;
	String partDesc;
	int itmQnty;
	double itmPrice;

	public Invoice(String partNo, String partDesc, int itmQnty, double itmPrice) {

		this.partNo = partNo;
		this.partDesc = partDesc;
		this.itmQnty = itmQnty;
		this.itmPrice = itmPrice;
	}

	public String getPartNo() {
		return partNo;
	}

	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}

	public String getPartDesc() {
		return partDesc;
	}

	public void setPartDesc(String partDesc) {
		this.partDesc = partDesc;
	}

	public int getItmQnty() {
		return itmQnty;
	}

	public void setItmQnty(int itmQnty) {
		
		this.itmQnty = itmQnty;
		if (itmQnty < 0) {
			itmQnty = 0;

		}
	}

	public double getItmPrice() {
		return itmPrice;
	}

	public void setItmPrice(double itmPrice) {
		this.itmPrice = itmPrice;

	}

	double getInvoice_amount() {
		double invceAmnt = itmPrice * itmQnty;

		return invceAmnt;

	}

}
