package ap.wrk.animal;


public class TestAnimal {
	public static void main(String[] args) {
		Kitten kitten = new Kitten();
		TomCat tomcat = new TomCat();
		Dog dog = new Dog();
		
		
IAnimal[] array = new IAnimal[]{kitten, tomcat, dog};
		
		for (IAnimal animal : array){
			animal.printInfo();
		
		}
		
	}

}
