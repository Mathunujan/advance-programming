package dsa.wrk.two_d_array;

import java.util.Scanner;

public class MatrixMulDemo {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter raw and column: ");
		String input = sc.nextLine();

		int[] twoValues = new int[2];
		Scanner sc2 = new Scanner(input).useDelimiter(",");
		twoValues[0] = sc2.nextInt();
		twoValues[1] = sc2.nextInt();
		sc2.close();

		MatrixMultiple matrixMul = new MatrixMultiple(twoValues[0], twoValues[1]);

		int[][] matrixA = matrixMul.create2dArray();
		int[][] matrixB = matrixMul.create2dArray();

		System.out.println("-------------------------- Matrix A View---------------------------------------");
		matrixMul.printMatrix(matrixA);
		System.out.println("-------------------------- Matrix B View---------------------------------------");
		matrixMul.printMatrix(matrixB);
		System.out
				.println("-------------------------- Sum of Matrix A & B View---------------------------------------");
		matrixMul.printMatrix(matrixMul.mulMatrix(matrixA, matrixB));

	}
}
