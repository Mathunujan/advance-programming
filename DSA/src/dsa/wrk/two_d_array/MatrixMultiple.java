package dsa.wrk.two_d_array;

import java.util.Random;
import java.util.Scanner;

public class MatrixMultiple {
	static Scanner sc = new Scanner(System.in);
	int row;
	int col;
	static Random random = new Random();

	public MatrixMultiple(int row, int col) {

		this.row = row;
		this.col = col;
	}

	public int[][] create2dArray() {

		int matrixArray[][] = new int[row][col];

		for (int i = 0; i < matrixArray.length; i++) {
			for (int j = 0; j < matrixArray.length; j++) {
				int randV = random.nextInt(50);
				matrixArray[i][j] = randV;

				// int matrixValue=matrixA;

			}
			System.out.println();

		}

		return matrixArray;

	}

	public int[][] sumMatrix(int[][] matrixA, int[][] matrixB) {

		int sumArray[][] = new int[row][col];

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				sumArray[i][j] = matrixA[i][j] + matrixB[i][j];
				// int matrixValue=matrixA;

			}

		}

		return sumArray;

	}

	public void printMatrix(int[][] Matrix) {

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {

				System.out.print(" {" + Matrix[i][j] + "} ");
				// int matrixValue=matrixA;

			}
			System.out.println();

		}
		System.out.println();

	}

	public int[][] mulMatrix(int[][] matrixA, int[][] matrixB) {

		int sumArray[][] = new int[row][col];

		/*
		 * for (int i = 0; i < row; i++) { for (int j = 1; j < col; j++) {
		 * sumArray[j][i] = matrixA[i][i] * matrixB[i][i] + matrixA[i][j] *
		 * matrixB[j][i]; // int matrixValue=matrixA;
		 * 
		 * }
		 * 
		 * }
		 */

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				sumArray[i][j] = 0;
				for (int k = 0; k < col; k++) {
					sumArray[i][j] += matrixA[i][k] * matrixB[k][j];
				} // end of k loop

			}

			return sumArray;

		}
		return sumArray;
	}
}
